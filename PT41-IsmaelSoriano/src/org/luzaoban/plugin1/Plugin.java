package org.luzaoban.plugin1;

import cat.iam.pt41.IPluginTransformacio;

public class Plugin implements IPluginTransformacio {

	@Override
	public String transforma(String arg0) {
		return arg0.replace("c", "");
	}
	
	@Override
	public String getDescripcio() {
		return "Soc el plugin 1 i el meu mètode transforma extreu tots els caracters \"c\" que troba a la String";
	}

}
