package org.luzaoban.plugin3;

import cat.iam.pt41.IPluginTransformacio;

public class Plugin implements IPluginTransformacio {

	@Override
	public String transforma(String arg0) {
		return arg0.concat(", correcte! :)");
	}
	
	public String getDescripcio() {
		return "Soc el plugin 3 i el meu mètode transforma afegeix al final de la String la frase \", correcte! :)\"";
	}

}
