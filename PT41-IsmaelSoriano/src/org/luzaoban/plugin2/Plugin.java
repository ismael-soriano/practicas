package org.luzaoban.plugin2;

import cat.iam.pt41.IPluginTransformacio;

public class Plugin implements IPluginTransformacio {

	@Override
	public String transforma(String arg0) {
		return arg0.replaceAll("[aeiou]", "");
	}
	
	public String getDescripcio() {
		return "Soc el plugin 2 i el meu mètode transforma extreu totes les vocals que troba a la String";
	}

}
