package iesam.dam.m06.pt41.exercici1;

import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import cat.iam.pt41.IPluginTransformacio;

import java.awt.FlowLayout;

public class Exercici1GUI {
	public static final String PLUGIN_DIR = "plugins";
	private JFrame frame;
	private List<IPluginTransformacio> pluginsList;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Exercici1GUI window = new Exercici1GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Exercici1GUI() {
		pluginsList = new ArrayList<IPluginTransformacio>();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Exercici 1 - Ismael Soriano");
		frame.setBounds(100, 100, 350, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		/********* Text Area **************************************************/
		JTextArea textArea = new JTextArea(10,30);
		textArea.setMargin(new Insets(5, 5, 5, 5));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setText("String per defecte per fer comprovacions dels plugins");
		frame.getContentPane().add(textArea);

		/********* Load Button ************************************************/
		JButton loadButton = new JButton("Load");
		loadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Carregant plugins...");
				try {
					for (String jar : getJarFileList(PLUGIN_DIR))
						loadPlugin(PLUGIN_DIR + File.separator + jar);
				} catch (Exception ex) {
					System.err.println("Error al carregar els plugins: "
							+ ex.getMessage());
				}

				System.out.println("Plugins Carregats \n");
			}
		});
		frame.getContentPane().add(loadButton);

		/********* Executa Button *********************************************/
		JButton executaButton = new JButton("Executa");
		executaButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!pluginsList.isEmpty()) {
					System.out.println("Executant transformacions...");
					for (IPluginTransformacio plugin : pluginsList)
						System.out.println(plugin.transforma(textArea.getText()));
					System.out.println("Transformacions executades \n");
				}
			}
		});
		frame.getContentPane().add(executaButton);

		/********* Unload Button **********************************************/
		JButton unloadButton = new JButton("Unload");
		unloadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!pluginsList.isEmpty()) {
					System.out.println("Esborrant plugins...");
					pluginsList.removeAll(pluginsList);
					System.out.println("Plugins Esborrats \n");
				}
			}
		});
		frame.getContentPane().add(unloadButton);		
	}

	private List<String> getJarFileList(String path) {
		return new ArrayList<String>(Arrays.asList(new File(path).list()));
	}

	private void loadPlugin(String pathToJar) throws IOException,
			ClassNotFoundException, InstantiationException,
			IllegalAccessException {
		JarFile jarFile = new JarFile(pathToJar);
		Enumeration<JarEntry> e = jarFile.entries();

		URL[] urls = { new URL("jar:file:" + pathToJar + "!/") };
		URLClassLoader cl = URLClassLoader.newInstance(urls);

		while (e.hasMoreElements()) {
			JarEntry je = (JarEntry) e.nextElement();
			if (je.isDirectory() || !je.getName().endsWith(".class")) {
				continue;
			}
			// -6 because of .class
			String className = je.getName().substring(0,
					je.getName().length() - 6);
			className = className.replace('/', '.');

			if (className.contains("Plugin")) {
				Class<?> c = cl.loadClass(className);
				IPluginTransformacio plugin = (IPluginTransformacio) c
						.newInstance();
				System.out.println(plugin.toString() + " - "
						+ plugin.getDescripcio());
				pluginsList.add(plugin);
			}

		}

		jarFile.close();
	}

}
