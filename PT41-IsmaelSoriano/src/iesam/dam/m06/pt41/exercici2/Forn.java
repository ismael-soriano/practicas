package iesam.dam.m06.pt41.exercici2;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;

public class Forn implements IForn {
	public static final String PROP_HUMITAT = "humitat";
	public static final String PROP_TEMP = "temperatura";
	private int temperatura;
	private int humitat;
	private List<IControlador> temperaturaListener;
	private List<IControlador> humitatListener;
	
	public Forn() {
		temperaturaListener = new ArrayList<IControlador>();
		humitatListener = new ArrayList<IControlador>();
	}

	@Override
	public void setTemperatura(int temp) {
		int oldValue = temperatura;
		
		try {
			for (IControlador controladorTemperatura : temperaturaListener) {
				controladorTemperatura.vetoableChange(new PropertyChangeEvent(this, PROP_TEMP, oldValue, temp));
			}
			
			this.temperatura = temp;
		} catch (PropertyVetoException e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public int getTemperatura() {
		return temperatura;
	}

	@Override
	public void setHumitat(int humitat) {
		int oldValue = this.humitat;
		
		try {
			for (IControlador controladorHumitat : humitatListener) {
				controladorHumitat.vetoableChange(new PropertyChangeEvent(this, PROP_HUMITAT, oldValue, humitat));
			}
			
			this.humitat = humitat;
		} catch (PropertyVetoException e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public int getHumitat() {
		return humitat;
	}

	@Override
	public void addTemperaturaListener(IControlador controlador) {
		temperaturaListener.add(controlador);
	}

	@Override
	public void removeTemperaturaListener(IControlador controlador) {
		temperaturaListener.remove(controlador);
	}

	@Override
	public void addHumitatListener(IControlador controlador) {
		humitatListener.add(controlador);
	}

	@Override
	public void removeHumitatListener(IControlador controlador) {
		humitatListener.remove(controlador);
	}

}
