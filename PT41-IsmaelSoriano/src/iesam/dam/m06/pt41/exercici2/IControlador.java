package iesam.dam.m06.pt41.exercici2;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

public interface IControlador extends VetoableChangeListener {

	/**
	 * Configura el nivell màxim que aquest controlador permetrà
	 * 
	 * @param max
	 *            Un enter que indica el nivel màxim que no vetarà
	 */
	public void setMax(int max);

	// Aquest mètode serà executat automàticament quan canviï la propietat del
	// forn.
	// S'han de vetar els canvis que facin que el nou nivell superi el màxim
	// configurat.

	// A més a més, s'ha d'imprimir per consola un text com el següent quan hi
	// hagi un vet

	// "Nom classe contolador: s'ha vetat un canvi de 600 a 800 perque supera el
	// limit de 700.
	// on Nom classe controlador es refereix al nom real de la classe que has
	// creat que implementa IControlador

	@Override
	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException;
}
