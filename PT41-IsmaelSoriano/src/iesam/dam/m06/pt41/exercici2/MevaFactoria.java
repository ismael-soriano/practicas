package iesam.dam.m06.pt41.exercici2;

public class MevaFactoria implements IFactoriaEmpresa {

	@Override
	public IForn createForn() {
		return new Forn();
	}

	@Override
	public IControlador createControlador(int max) {
		IControlador controlador = new Controlador();
		controlador.setMax(max);
		return controlador;
	}

}
