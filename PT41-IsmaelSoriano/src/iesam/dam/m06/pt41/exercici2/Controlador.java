package iesam.dam.m06.pt41.exercici2;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;

public class Controlador implements IControlador {
	private int max;

	@Override
	public void setMax(int max) {
		this.max = max;
	}

	@Override
	public void vetoableChange(PropertyChangeEvent evt)
			throws PropertyVetoException {
		int newValue = (Integer) evt.getNewValue();

		if (newValue > max) {
			throw new PropertyVetoException(
					"Controlador: s'ha vetat un canvi de " + evt.getOldValue()
							+ " a " + newValue + " perque supera el limit de "
							+ max + ".", evt);
		}
	}

}
