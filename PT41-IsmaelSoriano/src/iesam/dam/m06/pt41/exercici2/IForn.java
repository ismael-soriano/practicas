package iesam.dam.m06.pt41.exercici2;


public interface IForn {

	/**
	 * Posa la temperatura actual del forn. Sempre que no sigui vetada per algun
	 * dels controladors
	 * Si es vetada, no s'ha de llançar cap excepció cap a dalt, sinó capturar-la aquí.
	 * 
	 * @param temp
	 *            La temperatura nova
	 * 
	 */
	public void setTemperatura(int temp)  ;

	/**
	 * Retorna la temperatura actual
	 * 
	 * @return Un enter amb la temperatura actual
	 */
	public int getTemperatura();

	/**
	 * Posa la humitat actual del forn. Sempre que no sigui vetada per algun
	 * dels controladors
	 * Si es vetada, no s'ha de llançar cap excepció cap a dalt, sinó capturar-la aquí.
	 * 
	 * @param temp
	 *            La humitat nova
	 *
	 */
	public void setHumitat(int humitat) ;

	/**
	 * Retorna la temperatura actual
	 * 
	 * @return Un enter amb la temperatura actual
	 */
	public int getHumitat();

	/**
	 * Afegeix el controlador per a que sigui avisat quan canviï la propietat de
	 * la temperatura
	 * 
	 * @param controlador
	 *            El controlador que solicita ser avisat
	 */
	public void addTemperaturaListener(IControlador controlador);

	/**
	 * Elimina el controlador dels que seran avisats quan canviï la propietat.
	 * tempertaura
	 * 
	 * @param controlador
	 *            El controlador que solicita no ser avisat
	 */
	public void removeTemperaturaListener(IControlador controlador);

	/**
	 * Afegeix el controlador per a que sigui avisat quan canviï la propietat de
	 * la temperatura
	 * 
	 * @param controlador
	 *            El controlador que solicita ser avisat
	 */
	public void addHumitatListener(IControlador controlador);

	/**
	 * Elimina el controlador dels que seran avisats quan canviï la propietat
	 * tempertaura
	 * 
	 * @param controlador
	 *            El controlador que solicita no ser avisat
	 */
	public void removeHumitatListener(IControlador controlador);

}
