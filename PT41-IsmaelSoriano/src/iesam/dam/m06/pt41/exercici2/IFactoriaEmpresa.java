package iesam.dam.m06.pt41.exercici2;

public interface IFactoriaEmpresa {
	/**
	 * Crea un forn
	 * 
	 * @return El forn
	 */
	public IForn createForn();

	/**
	 * Crea un nou controlador. Aquest controlador tindrà configurat el màxim
	 * nivell a partir del qual vetarà els canvis
	 * 
	 * @param max
	 *            El màxim nivell que permetrà. Nivells superiors faran vetar
	 *            els canvis.
	 * @return El controlador creat.
	 */
	public IControlador createControlador(int max);

}
