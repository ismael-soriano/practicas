package iesam.dam.m06.pt41.exercici2;

import java.util.Scanner;


public class Principal {

	//CANVIA AQUESTA STRING AMB EL NOM DE LA TEVA CLASSE FACTORIA
	static String nomClasse = "iesam.dam.m06.pt41.exercici2.MevaFactoria"; //"cat.iam.ad.pt41.exercici2.solucio.MevaFactoria";

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Scanner teclat = new Scanner(System.in);
		System.out.println("Escriu el nom de la classe de la teva factoria. \n Intro per a utilitzar " + nomClasse );
		String input = teclat.nextLine();

		if (!input.equals("")) {
			nomClasse = input;
		}

		IFactoriaEmpresa factoria = (IFactoriaEmpresa) Class.forName(nomClasse).newInstance();
		IForn forn = factoria.createForn();

		IControlador controlT1 = factoria.createControlador(800);
		IControlador controlT2 = factoria.createControlador(750);
		IControlador controlT3 = factoria.createControlador(800);
		IControlador controlH1 = factoria.createControlador(50);
		IControlador controlH2 = factoria.createControlador(55);

		forn.addTemperaturaListener(controlT1);
		forn.addTemperaturaListener(controlT2);
		forn.addTemperaturaListener(controlT3);

		forn.addHumitatListener(controlH1);
		forn.addHumitatListener(controlH2);

		System.out.println("\n1 - No s'ha de veure cap vet");
		forn.setHumitat(10);
		forn.setTemperatura(200);
		System.out.println("Estat inicial: temp= " + forn.getTemperatura() + " humitat= " + forn.getHumitat());

		System.out.println("\n2 - S'ha de veure un vet en la humitat");
		forn.setHumitat(60);
		forn.setTemperatura(200);
		System.out.println("Estat forn: temp= " + forn.getTemperatura() + " humitat= " + forn.getHumitat());

		System.out.println("\n3 - S'ha de veure un vet en la temperatura");
		forn.setHumitat(32);
		forn.setTemperatura(900);
		System.out.println("Estat forn: temp= " + forn.getTemperatura() + " humitat= " + forn.getHumitat());

		forn.removeHumitatListener(controlH1);
		forn.removeHumitatListener(controlH2);

		System.out.println("\n4 - No s'h de veure res perquè ja ningú veta els canvis d'humitat");
		forn.setHumitat(1000);
		forn.setTemperatura(200);
		System.out.println("Estat forn: temp= " + forn.getTemperatura() + " humitat= " + forn.getHumitat());

		teclat.close();
	}

}
