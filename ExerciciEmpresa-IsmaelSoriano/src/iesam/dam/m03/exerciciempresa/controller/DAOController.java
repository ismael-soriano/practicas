package iesam.dam.m03.exerciciempresa.controller;

import iesam.dam.m03.exerciciempresa.models.Departament;
import iesam.dam.m03.exerciciempresa.models.DetallsEmpleat;
import iesam.dam.m03.exerciciempresa.models.Empleat;
import iesam.dam.m03.exerciciempresa.models.Producte;
import iesam.dam.m03.exerciciempresa.models.dao.DepartamentDAO;
import iesam.dam.m03.exerciciempresa.models.dao.DepartamentDAOImpl;
import iesam.dam.m03.exerciciempresa.models.dao.DetallsEmpleatDAO;
import iesam.dam.m03.exerciciempresa.models.dao.DetallsEmpleatDAOImpl;
import iesam.dam.m03.exerciciempresa.models.dao.EmpleatDAO;
import iesam.dam.m03.exerciciempresa.models.dao.EmpleatDAOImpl;
import iesam.dam.m03.exerciciempresa.models.dao.ProducteDAO;
import iesam.dam.m03.exerciciempresa.models.dao.ProducteDAOImpl;
import iesam.dam.m03.exerciciempresa.ui.CreateDepartamentPanel;
import iesam.dam.m03.exerciciempresa.ui.CreateEmployeePanel;
import iesam.dam.m03.exerciciempresa.ui.CreateProductPanel;
import iesam.dam.m03.exerciciempresa.ui.MainFrame;
import iesam.dam.m03.exerciciempresa.utils.HibernateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

public class DAOController {
	public static boolean loaded;
	public static List<Empleat> empleatList;
	public static List<DetallsEmpleat> detallsEmpleatList;
	public static List<Departament> departamentList;
	public static List<Producte> producteList;
	private DepartamentDAO departamentDAO;
	private EmpleatDAO empleatDAO;
	private DetallsEmpleatDAO detallsEmpleatDAO;
	private ProducteDAO producteDAO;
	private MainFrame mainFrame;

	public void init() {
		loaded = false;
		HibernateUtils.buildSessionFactory();
		HibernateUtils.openSessionAndBindToThread();

		empleatDAO = new EmpleatDAOImpl();
		detallsEmpleatDAO = new DetallsEmpleatDAOImpl();
		departamentDAO = new DepartamentDAOImpl();
		producteDAO = new ProducteDAOImpl();

		refreshData();
		loaded = true;
	}

	public List<Object[]> getEmployeeList() {
		List<Object[]> employees = new ArrayList<Object[]>();
		for (Empleat empleat : empleatList) {
			employees.add(getEmployeeAsObject(empleat));
		}

		return employees;
	}

	public void createEmployee() {
		CreateEmployeePanel employeePanel = new CreateEmployeePanel();

		int result = JOptionPane.showConfirmDialog(null, employeePanel, "Crear Empleat",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			Empleat empleat = new Empleat();
			empleat.setNom(employeePanel.getNom());
			empleat.setCognom(employeePanel.getCognom());
			empleat.setIdDepartament(departamentDAO.findByName(employeePanel.getDepartament()).getId());
			empleat.setSalari(employeePanel.getSalari());
			empleat.setAniversari(employeePanel.getAniversari());
			empleat.setTelefon(employeePanel.getTelefon());
			empleat.setProductePreferit(producteDAO.findByName(employeePanel.getProductePreferit()));
			empleatDAO.save(empleat);
			mainFrame.addRow(getEmployeeAsObject(empleat));
			refreshData();

			JOptionPane
					.showMessageDialog(employeePanel, "S'ha creat el client");
		} else {
			JOptionPane.showMessageDialog(employeePanel,
					"No s'ha creat cap client.");
		}
	}
	
	public void createDepartment() {
		CreateDepartamentPanel departmentPanel = new CreateDepartamentPanel();

		int result = JOptionPane.showConfirmDialog(null, departmentPanel, "Crear Departament",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			Departament departament = new Departament();
			departament.setNom(departmentPanel.getNom());
			departament.setDescripcio(departmentPanel.getDescripcio());
			departamentDAO.save(departament);
			refreshData();

			JOptionPane
					.showMessageDialog(departmentPanel, "S'ha creat el departament");
		} else {
			JOptionPane.showMessageDialog(departmentPanel,
					"No s'ha creat cap departament.");
		}
	}
	
	public void createProducte() {
		CreateProductPanel productPanel = new CreateProductPanel();

		int result = JOptionPane.showConfirmDialog(null, productPanel, "Crear Producte",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			Producte producte = new Producte();
			producte.setNom(productPanel.getNom());
			producte.setDescripcio(productPanel.getDescripcio());
			producte.setPreu(productPanel.getPreu());
			producteDAO.save(producte);
			refreshData();

			JOptionPane
					.showMessageDialog(productPanel, "S'ha creat el producte");
		} else {
			JOptionPane.showMessageDialog(productPanel,
					"No s'ha creat cap producte.");
		}
	}
	
	public void refreshData() {
		empleatList = empleatDAO.findAll();
		departamentList = departamentDAO.findAll();
		detallsEmpleatList = detallsEmpleatDAO.findAll();
		producteList = producteDAO.findAll();
	}

	public Object[] getEmployeeAsObject(Empleat empleat) {
		int id = empleat.getId();
		String nom = empleat.getNom();
		String cognom = empleat.getCognom();
		int idDepartament = empleat.getIdDepartament();
		double salari = empleat.getSalari();
		Date aniversari = empleat.getAniversari();
		int telefon = empleat.getTelefon();
		String producteFavorit = empleat.getProductePreferit().getNom();

		return new Object[] { id, nom, cognom, idDepartament, salari,
				aniversari, telefon, producteFavorit };
	}

	public Object[] getDepartmentNames() {
		List<Object> departmentIds = new ArrayList<>();
		for (Departament departament : departamentList)
			departmentIds.add(departament.getNom());

		return departmentIds.toArray();
	}
	
	public Object[] getProductNames() {
		List<Object> productNames = new ArrayList<>();
		for (Producte producte : producteList)
			productNames.add(producte.getNom());

		return productNames.toArray();
	}

	public void setMainFrame(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	public void close() {
		empleatList = null;
		departamentList = null;
		detallsEmpleatList = null;

		empleatDAO = null;
		detallsEmpleatDAO = null;
		departamentDAO = null;

		HibernateUtils.closeSessionAndUnbindFromThread();
		HibernateUtils.closeSessionFactory();
	}

}
