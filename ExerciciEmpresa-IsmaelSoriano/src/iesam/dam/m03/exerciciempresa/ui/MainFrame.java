package iesam.dam.m03.exerciciempresa.ui;

import iesam.dam.m03.exerciciempresa.EmpresaManager;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class MainFrame extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 105563911638539275L;

	private static final String EMPLEAT_BUTTON = "Crear Empleat",
			DEPARTAMENT_BUTTON = "Crear Departament",
			PRODUCT_BUTTON = "Crear Producte";
	private JPanel contentPane;
	private JTable empleats;
	private DefaultTableModel empleatTableModel;
	private String[] empleatCols = { "ID", "Nom", "Cognom", "Departament",
			"Salari", "Aniversari", "Telefon", "Producte Preferit" };
	private JButton createEmployeeButton;

	public MainFrame() {
		EmpresaManager.controller.setMainFrame(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		empleats = new JTable();
		empleats.setFillsViewportHeight(true);
		JScrollPane scrollPane = new JScrollPane(empleats);
		contentPane.add(scrollPane, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel(new GridLayout(1, 0));
		contentPane.add(buttonsPanel, BorderLayout.SOUTH);

		createEmployeeButton = new JButton(EMPLEAT_BUTTON);
		createEmployeeButton.addActionListener(this);
		buttonsPanel.add(createEmployeeButton);

		JButton createDepartmentButton = new JButton(DEPARTAMENT_BUTTON);
		createDepartmentButton.addActionListener(this);
		buttonsPanel.add(createDepartmentButton);
		
		JButton createProducttButton = new JButton(PRODUCT_BUTTON);
		createProducttButton.addActionListener(this);
		buttonsPanel.add(createProducttButton);

		initData();
	}

	public void initData() {
		empleatTableModel = new DefaultTableModel(empleatCols, 0) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 3634130727136344084L;

			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		for (Object[] employee : EmpresaManager.controller.getEmployeeList())
			empleatTableModel.addRow(employee);

		empleats.setModel(empleatTableModel);
	}

	public void addRow(Object[] employee) {
		empleatTableModel.addRow(employee);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();

		switch (button.getText()) {
		case EMPLEAT_BUTTON:
			EmpresaManager.controller.createEmployee();
			break;

		case DEPARTAMENT_BUTTON:
			EmpresaManager.controller.createDepartment();
			break;
			
		case PRODUCT_BUTTON:
			EmpresaManager.controller.createProducte();
			break;
		}

	}
}
