package iesam.dam.m03.exerciciempresa.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class CreateDepartamentPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7274837547790915300L;
	private JTextField nom;
	private JTextArea descripcio;

	/**
	 * Create the panel.
	 */
	public CreateDepartamentPanel() {
		nom = new JTextField();
		descripcio = new JTextArea(5,1);
		descripcio.setLineWrap(true);
		JScrollPane scrollPane = new JScrollPane(descripcio);
		
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		add(new JLabel("Nom:"), gbc);
		
		gbc.gridy = 1;
		add(nom, gbc);
		
		gbc.gridy = 2;
		add(new JLabel("Descripció:"), gbc);
		
		gbc.gridy = 3;
		gbc.weighty = 5;
		add(scrollPane, gbc);
	}
	
	public String getNom() {
		return nom.getText();
	}
	
	public String getDescripcio() {
		return descripcio.getText();
	}

}
