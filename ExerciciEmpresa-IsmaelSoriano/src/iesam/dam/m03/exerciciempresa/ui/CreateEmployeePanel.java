package iesam.dam.m03.exerciciempresa.ui;

import iesam.dam.m03.exerciciempresa.EmpresaManager;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

@SuppressWarnings("rawtypes")
public class CreateEmployeePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5944092294028775505L;
	private JTextField nom, cognom, salari, telefon, carrer, ciutat, estat, pais;
	private JComboBox idDepartament, productePreferit;
	private JDatePickerImpl datePicker;

	/**
	 * Create the frame.
	 */
	public CreateEmployeePanel() {
		setLayout(new BorderLayout());
		initComponents();

		GridLayout gl = new GridLayout(0, 1);
		
		JPanel empleat = new JPanel();
		empleat.setLayout(gl);
		empleat.add(new JLabel("Nom:"));
		empleat.add(nom);
		empleat.add(new JLabel("Cognom:"));
		empleat.add(cognom);
		empleat.add(new JLabel("Departament:"));
		empleat.add(idDepartament);
		empleat.add(new JLabel("Salari:"));
		empleat.add(salari);
		empleat.add(new JLabel("Aniversari:"));
		empleat.add(datePicker);
		empleat.add(new JLabel("Telefon:"));
		empleat.add(telefon);
		empleat.add(new JLabel("Producte preferit:"));
		empleat.add(productePreferit);
		
		JPanel detallsEmpleat = new JPanel();
		detallsEmpleat.setLayout(gl);
		detallsEmpleat.add(new JLabel("Carrer:"));
		detallsEmpleat.add(carrer);
		detallsEmpleat.add(new JLabel("Ciutat:"));
		detallsEmpleat.add(ciutat);
		detallsEmpleat.add(new JLabel("Estat:"));
		detallsEmpleat.add(estat);
		detallsEmpleat.add(new JLabel("Pais:"));
		detallsEmpleat.add(pais);
		

		JTabbedPane tp = new JTabbedPane();
		tp.add("Empleat", empleat);
		tp.add("Detalls", detallsEmpleat);
		
		
		add(tp, BorderLayout.CENTER);
	}

	private void initComponents() {
		UtilDateModel model = new UtilDateModel();
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());

		idDepartament = new JComboBox<>(
				EmpresaManager.controller.getDepartmentNames());
		productePreferit = new JComboBox<>(
				EmpresaManager.controller.getProductNames());

		nom = new JTextField();
		cognom = new JTextField();
		salari = new JTextField();
		telefon = new JTextField();
		carrer = new JTextField();
		ciutat = new JTextField();
		estat = new JTextField();
		pais = new JTextField();
	}

	public class DateLabelFormatter extends AbstractFormatter {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4660471532727192256L;
		private final String datePattern = "yyyy-MM-dd";
		private final SimpleDateFormat dateFormatter = new SimpleDateFormat(
				datePattern);

		@Override
		public Object stringToValue(String text) throws ParseException,
				java.text.ParseException {
			return dateFormatter.parseObject(text);
		}

		@Override
		public String valueToString(Object value) throws ParseException {
			if (value != null) {
				Calendar cal = (Calendar) value;
				return dateFormatter.format(cal.getTime());
			}

			return "";
		}

	}

	public String getNom() {
		return nom.getText();
	}

	public String getCognom() {
		return cognom.getText();
	}

	public double getSalari() {
		return Double.parseDouble(salari.getText());
	}

	public int getTelefon() {
		return Integer.parseInt(telefon.getText());
	}

	public String getDepartament() {
		return (String) idDepartament.getSelectedItem();
	}

	public Date getAniversari() {
		return (Date) datePicker.getModel().getValue();
	}

	public String getProductePreferit() {
		return (String) productePreferit.getSelectedItem();
	}

}
