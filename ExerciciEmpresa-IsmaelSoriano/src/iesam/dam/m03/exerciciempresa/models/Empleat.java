package iesam.dam.m03.exerciciempresa.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name="EMPLEAT")
public class Empleat {
	@Id
	@GeneratedValue
	@Column(name="Empleat_Id")
	private int id;
	
	@Column(name="Empleat_Nom")
	private String nom;
	
	@Column(name="Empleat_Cognom")
	private String cognom;
	
	@Column(name="Empleat_Salari")
	private double salari;
	
	@Column(name="Empleat_Aniversari")
	private Date aniversari;
	
	@Column(name="Empleat_Telefon")
	private int telefon;
	
	@JoinColumn(name="Departament_Id")
	private int idDepartament;
	
	@OneToOne
	@JoinColumn(name="Producte_Nom")
	private Producte productePreferit;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public double getSalari() {
		return salari;
	}

	public void setSalari(double salari) {
		this.salari = salari;
	}

	public Date getAniversari() {
		return aniversari;
	}

	public void setAniversari(Date aniversari) {
		this.aniversari = aniversari;
	}

	public int getTelefon() {
		return telefon;
	}

	public void setTelefon(int telefon) {
		this.telefon = telefon;
	}

	public int getIdDepartament() {
		return idDepartament;
	}

	public void setIdDepartament(int idDepartament) {
		this.idDepartament = idDepartament;
	}

	@Override
	public String toString() {
		return "Empleat [id=" + id + ", nom=" + nom + ", cognom=" + cognom
				+ ", salari=" + salari + ", aniversari=" + aniversari
				+ ", telefon=" + telefon + ", idDepartament=" + idDepartament
				+ "]";
	}

	public Producte getProductePreferit() {
		return productePreferit;
	}

	public void setProductePreferit(Producte productePreferit) {
		this.productePreferit = productePreferit;
	}

}
