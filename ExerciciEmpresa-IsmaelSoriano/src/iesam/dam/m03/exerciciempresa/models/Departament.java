package iesam.dam.m03.exerciciempresa.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="DEPARTAMENT")
public class Departament {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="Departament_Id", unique=true)
	private int id;
	
	@Column(name="Departament_Nom")
	private String nom;
	
	@Column(name="Departament_Descripcio")
	private String descripcio;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	@Override
	public String toString() {
		return "Departament [id=" + id + ", nom=" + nom + ", descripcio="
				+ descripcio + "]";
	}
}
