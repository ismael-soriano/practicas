package iesam.dam.m03.exerciciempresa.models.dao;

import iesam.dam.m03.exerciciempresa.models.Departament;
import iesam.dam.m03.exerciciempresa.models.Producte;
import iesam.dam.m03.exerciciempresa.utils.HibernateUtils;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

@SuppressWarnings("unchecked")
public class DepartamentDAOImpl implements DepartamentDAO {

	@Override
	public List<Departament> findAll() {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();
		Criteria cr = session.createCriteria(Departament.class);
		return cr.list();
	}

	@Override
	public void save(Departament departament) {
		boolean exists = isInDataBase(departament);

		if (!exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.save(departament);
			session.getTransaction().commit();
		}
	}
	
	@Override
	public Departament findByName(String name) {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();

		Criteria cr = session.createCriteria(Producte.class);
		cr.add(Restrictions.eq("nom", name));
		return (Departament) cr.list().get(0);
	}

	@Override
	public void update(Departament departament) {
		boolean exists = isInDataBase(departament);

		if (exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.update(departament);
			session.getTransaction().commit();
		}
	}

	@Override
	public void delete(Departament departament) {
		boolean exists = isInDataBase(departament);

		if (exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.delete(departament);
			session.getTransaction().commit();
		}
	}

	private boolean isInDataBase(Departament departament) {
		List<Departament> departaments = findAll();
		for (Departament departamentDB : departaments)
			if (departamentDB.getNom().equalsIgnoreCase(departament.getNom()))
				return true;
		return false;
	}

}
