package iesam.dam.m03.exerciciempresa.models.dao;

import iesam.dam.m03.exerciciempresa.models.Producte;
import iesam.dam.m03.exerciciempresa.utils.HibernateUtils;
import iesam.dam.m03.exerciciempresa.utils.Constants.QueryOrder;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@SuppressWarnings("unchecked")
public class ProducteDAOImpl implements ProducteDAO {

	@Override
	public List<Producte> findAll() {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();
		Criteria cr = session.createCriteria(Producte.class);
		return cr.list();
	}

	@Override
	public List<Producte> findAllOrderedPrice(QueryOrder order) {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();
		Criteria cr = session.createCriteria(Producte.class);
		
		switch(order) {
		case ASC:
			cr.addOrder(Order.asc("preu"));
			break;
			
		case DESC:
			cr.addOrder(Order.desc("preu"));
			break;
		}
		
		return cr.list();
	}

	@Override
	public Producte findById(int id) {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();

		Criteria cr = session.createCriteria(Producte.class);
		cr.add(Restrictions.eq("id", id));
		return (Producte) cr.list().get(0);
	}

	@Override
	public Producte findByName(String name) {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();

		Criteria cr = session.createCriteria(Producte.class);
		cr.add(Restrictions.eq("nom", name));
		return (Producte) cr.list().get(0);
	}

	@Override
	public void save(Producte producte) {
		boolean exists = isInDataBase(producte);

		if (!exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.save(producte);
			session.getTransaction().commit();
		}
	}

	@Override
	public void update(Producte producte) {
		boolean exists = isInDataBase(producte);

		if (exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.update(producte);
			session.getTransaction().commit();
		}
	}

	@Override
	public void delete(Producte producte) {
		boolean exists = isInDataBase(producte);

		if (exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.delete(producte);
			session.getTransaction().commit();
		}
	}

	private boolean isInDataBase(Producte producte) {
		List<Producte> productes = findAll();
		for (Producte producteDB : productes)
			if (producteDB.getNom().equalsIgnoreCase(producte.getNom()))
				return true;
		return false;
	}
}
