package iesam.dam.m03.exerciciempresa.models.dao;

import iesam.dam.m03.exerciciempresa.models.Empleat;

import java.util.List;

public interface EmpleatDAO {
	public List<Empleat> findAll();
	
	public List<Empleat> findMinorSalaryEmployee();
	
	public List<Empleat> findAllByDepartment(int department);
	
	public List<Empleat> findBySalari(double min, double max);
	
	public List<Empleat> findBySurnameBeginningWithA();
	
	public int findPhoneById(int id);
	
	public Empleat findById(int id);

	public void save(Empleat empleat);

	public void update(Empleat empleat);

	public void delete(Empleat empleat);
	
}
