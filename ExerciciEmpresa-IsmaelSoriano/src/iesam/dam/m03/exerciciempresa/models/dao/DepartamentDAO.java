package iesam.dam.m03.exerciciempresa.models.dao;

import iesam.dam.m03.exerciciempresa.models.Departament;

import java.util.List;

public interface DepartamentDAO {
	public List<Departament> findAll();
	
	public Departament findByName(String name);

	public void save(Departament departament);

	public void update(Departament departament);

	public void delete(Departament departament);
	
}
