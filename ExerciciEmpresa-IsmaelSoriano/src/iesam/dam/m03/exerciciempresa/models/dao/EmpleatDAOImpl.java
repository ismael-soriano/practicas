package iesam.dam.m03.exerciciempresa.models.dao;

import iesam.dam.m03.exerciciempresa.models.Empleat;
import iesam.dam.m03.exerciciempresa.utils.HibernateUtils;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

@SuppressWarnings("unchecked")
public class EmpleatDAOImpl implements EmpleatDAO {

	@Override
	public List<Empleat> findAll() {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();
		Criteria cr = session.createCriteria(Empleat.class);
		return cr.list();
	}

	@Override
	public List<Empleat> findMinorSalaryEmployee() {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();
		Criteria cr = session.createCriteria(Empleat.class);
		cr.add(Restrictions.eq("salari", Projections.min("salari")));
		return cr.list();
	}

	@Override
	public List<Empleat> findAllByDepartment(int department) {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();
		Criteria cr = session.createCriteria(Empleat.class);
		cr.add(Restrictions.eq("idDepartament", department));
		return cr.list();
	}

	@Override
	public int findPhoneById(int id) {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();

		Criteria cr = session.createCriteria(Empleat.class);
		cr.add(Restrictions.eq("id", id));
		return ((Empleat) cr.list().get(0)).getTelefon();
	}

	@Override
	public Empleat findById(int id) {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();

		Criteria cr = session.createCriteria(Empleat.class);
		cr.add(Restrictions.eq("id", id));
		return (Empleat) cr.list().get(0);
	}

	@Override
	public void save(Empleat empleat) {
		boolean exists = isInDataBase(empleat);

		if (!exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.save(empleat);
			session.getTransaction().commit();
		}
	}

	@Override
	public void update(Empleat empleat) {
		boolean exists = isInDataBase(empleat);

		if (exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.update(empleat);
			session.getTransaction().commit();
		}
	}

	@Override
	public void delete(Empleat empleat) {
		boolean exists = isInDataBase(empleat);

		if (exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.delete(empleat);
			session.getTransaction().commit();
		}
	}

	private boolean isInDataBase(Empleat empleat) {
		List<Empleat> empleats = findAll();
		for (Empleat empleatsDB : empleats)
			if (empleatsDB.getNom().equalsIgnoreCase(empleat.getNom())
					&& empleatsDB.getCognom().equalsIgnoreCase(empleat.getCognom()))
				return true;
		return false;
	}

	@Override
	public List<Empleat> findBySalari(double min, double max) {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();
		Criteria cr = session.createCriteria(Empleat.class);
		cr.add(Restrictions.between("Empleat_Salari", min, max));
		return cr.list();
	}

	@Override
	public List<Empleat> findBySurnameBeginningWithA() {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();
		Criteria cr = session.createCriteria(Empleat.class);
		cr.add(Restrictions.like("Empleat_Cognom", "A%"));
		return cr.list();
	}

}
