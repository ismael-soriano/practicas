package iesam.dam.m03.exerciciempresa.models.dao;

import iesam.dam.m03.exerciciempresa.models.Producte;
import iesam.dam.m03.exerciciempresa.utils.Constants.QueryOrder;

import java.util.List;

public interface ProducteDAO {
	public List<Producte> findAll();
	
	public List<Producte> findAllOrderedPrice(QueryOrder order);
	
	public Producte findById(int id);
	
	public Producte findByName(String name);

	public void save(Producte producte);

	public void update(Producte producte);

	public void delete(Producte producte);
}
