package iesam.dam.m03.exerciciempresa.models.dao;

import iesam.dam.m03.exerciciempresa.models.DetallsEmpleat;

import java.util.List;

public interface DetallsEmpleatDAO {
	public List<DetallsEmpleat> findAll();
	
	public void save(DetallsEmpleat detallsEmpleat);

	public void update(DetallsEmpleat detallsEmpleat);

	public void delete(DetallsEmpleat detallsEmpleat);
}
