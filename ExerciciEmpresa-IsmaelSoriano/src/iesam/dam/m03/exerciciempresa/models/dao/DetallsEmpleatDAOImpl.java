package iesam.dam.m03.exerciciempresa.models.dao;

import iesam.dam.m03.exerciciempresa.models.DetallsEmpleat;
import iesam.dam.m03.exerciciempresa.utils.HibernateUtils;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

@SuppressWarnings("unchecked")
public class DetallsEmpleatDAOImpl implements DetallsEmpleatDAO {

	@Override
	public List<DetallsEmpleat> findAll() {
		Session session = HibernateUtils.getSessionFactory()
				.getCurrentSession();
		Criteria cr = session.createCriteria(DetallsEmpleat.class);
		return cr.list();
	}

	@Override
	public void save(DetallsEmpleat detallsEmpleat) {
		boolean exists = isInDataBase(detallsEmpleat);

		if (!exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.save(detallsEmpleat);
			session.getTransaction().commit();
		}
	}

	@Override
	public void update(DetallsEmpleat detallsEmpleat) {
		boolean exists = isInDataBase(detallsEmpleat);

		if (exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.update(detallsEmpleat);
			session.getTransaction().commit();
		}
	}

	@Override
	public void delete(DetallsEmpleat detallsEmpleat) {
		boolean exists = isInDataBase(detallsEmpleat);

		if (exists) {
			Session session = HibernateUtils.getSessionFactory()
					.getCurrentSession();

			session.beginTransaction();
			session.delete(detallsEmpleat);
			session.getTransaction().commit();
		}
	}

	private boolean isInDataBase(DetallsEmpleat detallsEmpleat) {
		List<DetallsEmpleat> detallsEmpleats = findAll();
		for (DetallsEmpleat detallsEmpleatDB : detallsEmpleats)
			if (detallsEmpleatDB.getCiutat().equalsIgnoreCase(
					detallsEmpleat.getCiutat())
					&& detallsEmpleatDB.getCarrer().equalsIgnoreCase(
							detallsEmpleat.getCarrer())
					&& detallsEmpleatDB.getEstat().equalsIgnoreCase(
							detallsEmpleat.getEstat()))
				return true;
		return false;
	}

}
