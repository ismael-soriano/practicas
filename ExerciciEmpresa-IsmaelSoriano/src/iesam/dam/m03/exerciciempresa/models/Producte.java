package iesam.dam.m03.exerciciempresa.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="PRODUCTE")
public class Producte {
	@Id
	@GeneratedValue
	@Column(name="Producte_Id")
	private int id;
	
	@Column(name="Producte_Nom")
	private String nom;
	
	@Column(name="Producte_Descripcio")
	private String descripcio;
	
	@Column(name="Producte_Preu")
	private double preu;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getPreu() {
		return preu;
	}

	public void setPreu(double preu) {
		this.preu = preu;
	}

	@Override
	public String toString() {
		return "Producte [id=" + id + ", nom=" + nom + ", descripcio="
				+ descripcio + ", preu=" + preu + "]";
	}
	
}
