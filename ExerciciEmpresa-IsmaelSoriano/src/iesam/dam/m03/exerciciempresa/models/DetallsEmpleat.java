package iesam.dam.m03.exerciciempresa.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity(name="DETALLS_EMPLEAT")
public class DetallsEmpleat {
	@Id
	@GeneratedValue
	@JoinColumn(name="Empleat_Id")
	private int id;
	
	@Column(name="Detalls_Empleat_Carrer")
	private String carrer;
	
	@Column(name="Detalls_Empleat_Ciutat")
	private String ciutat;
	
	@Column(name="Detalls_Empleat_Estat")
	private String estat;
	
	@Column(name="Detalls_Empleat_Pais")
	private String pais;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCarrer() {
		return carrer;
	}

	public void setCarrer(String carrer) {
		this.carrer = carrer;
	}

	public String getCiutat() {
		return ciutat;
	}

	public void setCiutat(String ciutat) {
		this.ciutat = ciutat;
	}

	public String getEstat() {
		return estat;
	}

	public void setEstat(String estat) {
		this.estat = estat;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	@Override
	public String toString() {
		return "DetallsEmpleat [id=" + id + ", carrer=" + carrer + ", ciutat="
				+ ciutat + ", estat=" + estat + ", pais=" + pais + "]";
	}
}
