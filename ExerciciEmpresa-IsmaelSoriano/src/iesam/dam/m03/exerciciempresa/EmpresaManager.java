package iesam.dam.m03.exerciciempresa;

import iesam.dam.m03.exerciciempresa.controller.DAOController;
import iesam.dam.m03.exerciciempresa.ui.MainFrame;

import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class EmpresaManager extends WindowAdapter {

	public static DAOController controller;
	private MainFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmpresaManager window = new EmpresaManager();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EmpresaManager() {
		controller = new DAOController();
		controller.init();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new MainFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setTitle("Empresa Manager");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	@Override
	public void windowClosing(WindowEvent e) {
		controller.close();
	}

}
